#!/bin/bash
CARLA_inbound_simulator='scripts/0_ego_knowledge_multicontrol_inbound.py'
CARLA_outbound_simulator='scripts/1_ego_knowledge_multicontrol_outbound_ml.py'
CARLA_outbound_simulator_add='scripts/2_ego_knowledge_multicontrol_outbound_ml_add.py'
python3 ${CARLA_inbound_simulator} &
python3 ${CARLA_outbound_simulator} &
python3 ${CARLA_outbound_simulator_add}
