# Comfort-based AI-driven Roundabout Control for Automated Vehicles

In this work, we propose an AI-driven roundabout control considering passenger comfort metrics, such as acceleration and jerk limitations.

<img src="images/decision_arc.png" width="720" height="400">



## Contents

*   [Deployement requirements](#deployement-requirements)
*   [Getting started](#getting-started)
    *   [Roundabout-inbound control](#roundabout-inbound-control)
    *   [Roundabout-outbound control using AI](#roundabout-outbound-controlcusing-ai)
*   [Used Technologies](#used-technologies)
*   [Contribute](#contribute)
*   [License](#license)
*   [Citation](#citation)

## Deployement requirements

*   [x] <strong>[CARLA](https://docs.docker.com/engine/) </strong> 0.9.10 
*   [x] <strong> Python </strong>  3.7


## Getting Started

Clone the master branch of this repository to your local machine:

```
git clone -b master  https://gitlab.eurecom.fr/cats/carla/ai-driven-roundabout-control.git
```

<strong>NB:</strong> Change environnement file .env as you prefere
<table>
<tr><td colspan=3 align="center"> <strong>.env</strong></td></tr>
<tr><td>Environment Variable</td><td>Default</td><td>Description</td></tr>
<tr><td>CLIENT_HOST</td><td>127.0.0.1</td><td><em>IP of CARLA client machine</td></tr>
<tr><td>CLIENT_PORT</td><td>2000</td><td><em>Port for CARLA client connection</td></tr>
<tr><td>TRAFFICMANAGER_PORT</td><td>8883</td><td><em>Port for traffic manager control</td></tr>
</table>


```bash
$ ./run.sh
```
This shell starts 2 CARLA clients, first one to control vehicles running inside roundabout and the second to control the approaching vehicle toward roundabout. We splited the simulation between 2 processes in order to keep smooth syncronisation between each client with server without and delay in frame update.

### 1. Roundabout-inbound control

In this CARLA python script, we considered that Autonomous Vehicle keeps running inside Roundabout for all simulation time.

<!---![Alt Text](https://nextcloud.eurecom.fr/s/HjxZ6oywGZB7PMt/download/server.gif) --->

<hr />

### 2. Roundabout-outbound control using AI

In this CARLA python script, we considered that Autonomous Vehicle intends to enter the roundabout using pretrained AI model  (Roundabout-Exit-Probability model)

<br />

<img src="images/exit_prob.png" width="560" height="420">

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
This work is released under Apache 2.0 license. See the [license file](LICENSE.txt) for more details. Contribution and integrations are appreciated.

## Citation

If you use this work for your Research, please cite the paper it is described in:

> __DProj: Comfort-based AI-driven Roundabout Control for Automated Vehicles
>
> Sina Moradi, Ali Nadar, Jérôme Härri 
>
> MT-ITS 2023.06.13.00001; doi: https://ieee.org/TBD

