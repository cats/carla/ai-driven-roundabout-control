#region import
import glob
from datetime import datetime
import traceback
#from threading import Thread
import threading
import subprocess
import pygame
import os
import sys
#from solver import *
import time
import json
import csv

import time
import pickle
import numpy as np
####from sklearn.linear_model import LogisticRegression
####from sklearn.model_selection import train_test_split
import random
import math
import argparse
import logging
import subprocess
from termcolor import colored
from dataclasses import dataclass
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt
#import torch
#import logisticRegression
#from logisticRegression import LogisticRegression
import random
time.sleep(6)
try:
    sys.path.append(glob.glob('../../../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    print("Failed to locate CARLA")
    pass
from time import gmtime, strftime
import carla
import circle_fit as cf
char = "*"
Delta_Time = 0.1
p = os.path.abspath('.')
sys.path.insert(1, p)
from methodes import *
from pid_test_update_ic_fcn_final_merge_2 import adv_thr_ctrl
from numpy import linalg as LA
array_crosstrack_error = []
import requests
from pathlib import Path
from dotenv import load_dotenv
#endregion
argparser = argparse.ArgumentParser(description=__doc__)
argparser.add_argument('--host',metavar='H', default='127.0.0.1', help='IP of the host server (default: 127.0.0.1)')
argparser.add_argument('--port',metavar='P', default=2000, type=int, help='TCP port to listen to (default: 2000)')
argparser.add_argument('--map',metavar='M', default='Town03', help='MAP to load simulator (default: Town03)')
argparser.add_argument('--threshold',metavar='T', default=0.5, help = "Exit probability threshod")
args = argparser.parse_args()
path = sys.path[0] + ('/../../ra_xodr.env' if args.map == "ra" else '/../../ra_town03.env')
#region Variables
PROB_THRESHOLD_EXIT = 0.5
PROB_THRESHOLD_STAY = 0.035
vehicles, new_points, polygones, Arcs, Zones,list_vehicleSpeeds = [],[], [], [], [], []
list_wps, ego_id, ego_actor, veh_ego, timestamp = None,None,None,None,None
k_e =  18.9
k_v =  2.2
MAX_EDGE = 80
nextInterval = 0.1
tm_port = 8000
total_veh_count = 10
safety_distance = 8
TARGET_SPEED = 6.95
SF = 1.1            # Safety Factor
BRAKE = 0.95
speed_limit = 30
spawn_back_at = 35
distance_attention = 15
distance_attention_margin = 2
speed_threshold = 8
steps_before_forcing = 1000
timeStep = 0
waitingTime = 150
initial_transform = None
step_ego_outZone = 0
step_ego_inZone = 0
total_wait = 0
success_wait = 0
failed_wait = 0
distance_threshold = 0
BRAKE_LIST = []
flag = False
dist_curvature_check = 5     # METERS, every 5 meters we check the curvature and the max allowable velocity based on lateral passenger comfort
dist_curvature_fit = 2      # METERS
num_point_circle_fit = int(dist_curvature_fit / nextInterval)
ACC_LATERAL = 0.15 * 9.81    # 0.15g
TARGET_VELOCITY = 40        # KM/H
LOOK_AHEAD_DIST = 25        # METERS
SPT = 3 # steer calculation per Throttle
CAUTION_DIST_BEFORE_BOX =10 # in meters
Distance_Ahead_Safety= 15
parking_x = -30
parking_y = 204
actors = []

model_name = 'town03.pickle'
model = pickle.load(open(model_name, 'rb'))
REP_stay_max = 0

#endregion
# class Point:
#     x: int
#     y:  int
#     def __init__(self,x,y):
#         self.x = x
#         self.y = y

@dataclass
class Exit:
    index: int
    name: str
    location: carla.Location

@dataclass
class Entry:
    index: int
    name: str
    location: carla.Location

exit_top = Exit(0, "TOP", carla.Location(x=TOP_exit_X, y=TOP_exit_Y, z=0.5))
exit_right = Exit(1, "RIGHT",carla.Location(x=RIGHT_exit_X, y=RIGHT_exit_Y, z=0.5))
exit_bottom = Exit(2, "BOTTOM",carla.Location(x=BOTTOM_exit_X, y=BOTTOM_exit_Y, z=0.5))
exit_left = Exit(3, "LEFT",carla.Location(x=LEFT_exit_X, y=LEFT_exit_Y, z=0.5))
Exits = [exit_top, exit_right, exit_bottom, exit_left]

entry_top = Entry(0, "TOP", carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5))
entry_right = Entry(1, "RIGHT",carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5))
entry_bottom = Entry(2, "BOTTOM",carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5))
entry_left = Entry(3, "LEFT",carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5))
Entries = [entry_top, entry_right, entry_bottom, entry_left]

RIGHT_SPOT = carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5)
TOP_SPOT = carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5)
LEFT_SPOT = carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5)
BOTTOM_SPOT = carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5)

box_right_entry = carla.Location(x=5.467874, y=34.947102, z=1)
box_top_entry = carla.Location(x=33.563801, y=-7.999302, z=1)
box_left_entry = carla.Location(x=-7.077684, y=-34.086342, z=1)
box_bottom_entry = carla.Location(x=-35.431053, y=1.172551, z=1)

box_right_exit = carla.Location(x=9.737114, y=23.112617, z=1)
box_top_exit = carla.Location(x=22.559887, y=-9.433374, z=1)
box_left_exit = carla.Location(x=-10.412357, y=-22.614059, z=1)
box_bottom_exit = carla.Location(x=-24.933035, y=5.580378, z=1)

# region Functions

def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude
#Calc minimum distance from a point and a line segment (i.e. consecutive vertices in a polyline).
def DistancePointLine(px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)

    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine

    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)

    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)

    return DistancePointLine

def get_nearest_wp(location, wps):
    old_d= float("inf")
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            wp = wps[i]
            index = i
    # if old_d > 2:
    #     return None, None
    #else:
    return index, wp

def get_nearest_index(t, wps, index_back,bumper):
    global flag
    direction = t.get_forward_vector()
    min_d= float("inf")
    index = 0
    my_x,my_y = bumper.x, bumper.y

    for i in range(index_back-5,index_back+6):
        if i < 0:continue
        elif i > len(wps)-2 : continue
        else:
            wp1, wp2 = wps[i], wps[i + 1]
            wp1_x, wp1_y = wp1.transform.location.x, wp1.transform.location.y
            wp2_x, wp2_y = wp2.transform.location.x, wp2.transform.location.y
            d = DistancePointLine(my_x, my_y, wp1_x, wp1_y, wp2_x, wp2_y)
            if d < min_d:
                min_d = d
                index = i
    return index

def getVehicleSpot_wp_index(list_wp, spot_location):
    distance = float("inf")
    index = 0
    counter = 0
    for i, wp in enumerate(list_wp):
        d = getDistance(wp.transform.location, spot_location)
        if d < distance:
            distance = d
            index = i

        if d > distance: counter +=1
        if counter > 3: break
    return index

def get_nearest_Vehicle(location):
    old_d= float('inf')
    for i in range(len(vehicles)):
        d = getDistance(location, vehicles[i].centroid)
        if d < old_d:
            old_d = d
    return old_d

def get_next_index(veh, wps, index):
    wp_location = wps[index].transform.location
    angle = find_angle(veh.direction,wp_location,veh.centroid)
    if abs(angle) < 90:
        return index
    else:
        return index+1

def getNearestExit(location) -> Exit:
    d = 1000000
    res_Exit = None
    for _exit in Exits:
        new_d = getDistance(_exit.location, location)
        if new_d < d:
            d = new_d
            res_Exit = _exit
    return res_Exit

# endregion
class Arc:
    back: carla.Location
    front: carla.Location
    symmetric: carla.Location
    def __init__(self, name, exit_back, exit_front):
        self.back = exit_back
        self.front = exit_front
        x = (self.back.x + self.front.x - CENTER.x)
        y = (self.back.y + self.front.y - CENTER.y)
        self.symmetric = carla.Location(x,y)

    def get_normalised_distance(self, bumper):
        # find_angle(getBumper(x), self.rear, self.bumper) < 10]
        # find_angle(getBumper(x), self.centroid, CENTER) < 10 and self.side == "left"]

        angle = find_angle( bumper, self.front,CENTER)
        angle = angle - 720 if angle > 600 else angle
        angle = - angle
        total = find_angle( self.back, self.front,CENTER)
        total = total - 720 if total > 600 else total
        total = - total

        #print(f"Angles: {round(angle)} / {round(total)} ")
        return angle/total


    def contains(self, location: carla.Location):
        point = Point(location.x, location.y)

        p_center = Point(CENTER.x, CENTER.y)
        p_back = Point(self.back.x, self.back.y)
        p_front = Point(self.front.x, self.front.y)
        p_symmetric = Point(self.symmetric.x, self.symmetric.y)

        polygon = Polygon([(p_center.x,p_center.y), \
                           (p_front.x,p_front.y), \
                            (p_symmetric.x,p_symmetric.y), \
                             (p_back.x, p_back.y)])
        return polygon.contains(point)


def get_safePoint_coor(location, distance):
    r= (R_SMALL + (LANE_WIDTH * 1.5 ))
    alpha = math.atan2(location.y-CENTER.y, location.x -CENTER.x)
    theta = distance/r
    angle = theta+alpha
    x = X_CENTER + r * math.cos(angle)
    y = Y_CENTER + r * math.sin(angle)
    return carla.Location(x=x, y=y, z=0.5)

class MyWaypoint(dict):
    radius: float
    v_max_turn : float
    circle_center = Point
    wp: carla.Waypoint
    transform: carla.Transform
    def __init__(self, wp):
        dict.__init__(self, wp=wp)
        self.wp = wp
        self.transform = wp.transform
        self.radius = float('inf')
        self.v_max_turn = 40    # km/h

@dataclass
class RotationLocation:
    roll: int
    yaw: int
    pitch: int
    x: int
    y: int
    z: int

@dataclass
class frontVehicle:
    distance_spot: float
    vehicle: object

@dataclass
class comingVehicle:
    distance_spot: float
    vehicle: object
    counter: int = 1

def vehicle_in_roundabout(location):
    dx = abs(location.x - CENTER.x)
    dy = abs(location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True
def bumper_in_roundabout(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    bumper_location = t.location + (actor.bounding_box.extent.x * fwd_vector)
    dx = abs(bumper_location.x - CENTER.x)
    dy = abs(bumper_location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True
def getBumper(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    bumper_location = t.location + (actor.bounding_box.extent.x * fwd_vector)
    return bumper_location
def getRear(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    rear_location = t.location - (actor.bounding_box.extent.x * fwd_vector)
    return rear_location

def getSpeed(actor):
    speed_x, speed_y = actor.get_velocity().x, actor.get_velocity().y
    speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
    speed_km = 3.6 * speed
    return [speed, speed_km]

def getLateral(actor):
    return round((getDistance(getBumper(actor), CENTER) - R_SMALL) / (LANE_COUNT * LANE_WIDTH), 3)

def getHeading(actor):
    fwd_vector =  actor.get_transform().get_forward_vector()
    steer = actor.get_control().steer
    bumper = getBumper(actor)
    steer_angle = steer *  (70 * (math.pi / 180))
    tangent = get_tangent(CENTER, bumper)
    direction = bumper + (10 * fwd_vector)
    angle_tangent_direction = find_angle(tangent, direction, bumper)
    return -(round(steer_angle + angle_tangent_direction, 2))

def getNormalisedDistance(actor):
    bumper = getBumper(actor)
    arc = None
    for i in range(len(Arcs)):
        if Arcs[i].contains(bumper):
            arc = Arcs[i]
            break
    return arc.get_normalised_distance(bumper)





class Vehicle(object):
    #region "Vehicle variables"
    _id: int
    transform: carla.Transform
    initial_transform: carla.Transform
    centroid: carla.Location
    bumper: carla.Location
    rear: carla.Location
    direction: carla.Location
    previous_location: carla.Location
    heading: float
    next_wp_index: int
    dist_exit: float
    dist_exit_normalized: float
    decision_dist: float
    decision_dist_norm: float
    decision_prob: float
    decision_speed: float
    dist_ra:float
    prev_next_exit: Exit = None
    prev_next_entry: Entry = None
    next_exit: Exit = None
    next_entry: Entry = None
    nearest_exit: Exit = None
    nearest_entry: Entry = None
    in_zone:bool = False
    in_box:bool = False
    traveled_distance: float
    centroid_in_ra: bool
    bumper_in_ra: bool
    rear_in_ra: bool
    bumper_to_ra:float
    rear_to_ra:float
    length: float
    prob_exit: float
    lateral: float
    steer_angle: float
    control: str
    wp_steer: float = 0
    wp_throttle: float = 0
    steer: float
    throttle: float
    brake: float
    brake_index: int =0
    speed: float
    speed_km: float
    angle_with_center:float
    list_speed_km:[]
    list_throttle:[]
    list_brake:[]
    front_vehicle: frontVehicle
    coming_vehicle: comingVehicle
    max_steering_angle: float
    tire_friction : float
    wheels_raduis: float
    max_brake_torque: float
    max_steering_angle_rad: float
    lane: int = None
    episode: int
    side:str
    target_zone_index: int
    target_zone_location: carla.Location
    slice: int = None
    status: str = "not-entered"
    arc: Arc = None
    zone: Arc = None
    arc_index_current: int = -1
    current_target_velocity = TARGET_VELOCITY
    throttle_index_current: int = 0
    throttle_list = []
    zone_index_current: int = -1
    arc_index_new: int = -1
    zone_index_new: int = -1
    lane_change: bool
    array_prediction = []
    array_dist_exit = []
    array_prob_exit = []
    array_distance = []
    array_probability = []
    wp: carla.Waypoint = None
    wp_index: int = None
    startTimestep: int = 0
    distance_decision_prediction: float = 0
    isExiting: bool = False
    spot_wp_index: int
    distance_toSpot: float
    time_entrybox_toSpot: float
    distance_entrybox_toSpot: float
    time_exitbox_toSpot: float
    distance_exitbox_toSpot: float
    time_toSpot: float
    spot_location : carla.Location
    list_wps =[]
    yield_to_veh: int
    yield_list = []
    yaw: float
    index_back: int
    index_front: int
    toward_RA : bool
    prev_in_ra:bool
    heading:float
    distance_by_angle = False
    calling_matlab = 0
    counter_steer = 0
    idx_box_entry = 0
    idx_box_exit = 0
    ahead = None
    isFollowing: False
    #endregion
    def __init__(self,side, actor, ego_wps=None):
        self._id = actor.id
        self.calling_matlab = 0
        self.counter_steer = 0
        self.wp_index = 0
        self.spot_wp_index = -1
        self.distance_toSpot = -1
        self.time_toSpot = -1
        self.prev_in_ra = False
        self.side = side
        if self.side == "right":
            self.target_zone_index = 2
            self.target_zone_location = carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5)
            self.spot_location = RIGHT_SPOT
        if self.side == "top":
            self.target_zone_index =1
            self.target_zone_location = carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5)
            self.spot_location = TOP_SPOT
        if self.side == "left":
            self.target_zone_index =0
            self.target_zone_location = carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5)
            self.spot_location = LEFT_SPOT
        if self.side == "bottom":
            self.target_zone_index =3
            self.target_zone_location = carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5)
            self.spot_location = BOTTOM_SPOT
        self.transform = actor.get_transform()
        self.previous_location = self.transform.location
        self.centroid = actor.get_location()
        fwd_vector = self.transform.get_forward_vector()
        self.bumper = self.centroid + (actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (actor.bounding_box.extent.x * fwd_vector)
        self.direction = self.bumper + (10 * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.length = getDistance(self.bumper, self.rear)
        self.traveled_distance = 0
        self.steps_total = 0
        self.next_wp_index = 1
        self.list_wps = []
        self.index_back = 0
        self.index_front = 0
        self.heading = 0
        self.dist_exit = None
        self.dist_exit_normalized = None
        self.dist_ra = None
        self.yield_to_veh = None
        self.yield_list = []
        self.decision_dist = None
        self.decision_dist_norm = None
        self.decision_prob = None
        self.decision_speed = None
        self.throttle = 0.5
        self.brake = 0
        self.centroid_in_ra = False
        self.bumper_in_ra = False
        self.rear_in_ra = False
        self.in_box = False
        self.lane_change = random.choice([False])
        self.prob_exit = 0
        self.episode = 1
        self.coming_vehicle: None
        self.current_target_velocity = TARGET_VELOCITY
        self.throttle_index_current = 0
        self.throttle_list = []
        self.array_prediction = []
        self.list_speed_km = []
        self.list_throttle = []
        self.list_brake = []
        self.array_dist_exit = []
        self.array_prob_exit = []
        physics_control = actor.get_physics_control()
        wheels = physics_control.wheels
        self.max_steering_angle = wheels[0].max_steer_angle
        self.tire_friction = wheels[0].tire_friction
        self.wheels_raduis = wheels[0].radius
        self.max_brake_torque = wheels[0].max_brake_torque
        self.max_steering_angle_rad = math.radians(self.max_steering_angle)
        self.front_vehicle = None
        self.angle_with_center = getAngleBetween2p(self.centroid, CENTER)
        #self.toward_RA = True if abs(self.angle_with_center-self.yaw) < 20 else False

    def update_Arc(self, debug):
        arc = None
        index = -1
        for i in range(len(Arcs)):
            if Arcs[i].contains(self.bumper):
                arc = Arcs[i]
                index = i
                break
        self.arc = arc
        self.prev_next_exit = self.next_exit
        self.next_exit = self.arc.front if self.arc is not None else None
        self.arc_index_new = index
    def update_Zone(self, debug):
        zone = None
        index = -1
        for i in range(len(Zones)):
            if Zones[i].contains(self.bumper, debug):
                zone = Zones[i]
                index = i
                break
        self.zone = zone
        if self.zone is not None:
            self.target_zone_location = self.zone.front.location
        self.prev_next_entry = self.next_entry
        self.next_entry = self.zone.front if self.zone is not None else None
        self.zone_index_new = index

    def update(self, actor,vehicles,world, debug):
        global main_exit ,distance_threshold, REP_stay_max
        global step_ego_outZone, step_ego_inZone
        self.transform = actor.get_transform()
        fwd_vector = self.transform.get_forward_vector()
        self.steer = actor.get_control().steer
        self.steer_angle = self.steer * self.max_steering_angle
        self.centroid = self.transform.location + carla.Location(z=1)
        self.bumper = self.centroid + (actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (actor.bounding_box.extent.x * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.centroid_in_ra = vehicle_in_roundabout(self.centroid)
        self.bumper_in_ra = vehicle_in_roundabout(self.bumper)

        self.rear_in_ra = vehicle_in_roundabout(self.rear)
        self.bumper_to_ra = getDistance(self.bumper,CENTER) - R_BIG
        self.rear_to_ra = getDistance(self.rear,CENTER) - R_BIG
        self.in_zone = (distance_attention_margin < self.bumper_to_ra < distance_attention and not self.bumper_in_ra)
        self.lateral = round((getDistance(self.centroid, CENTER) - R_SMALL) / (LANE_COUNT * LANE_WIDTH), 3)
        tangent = get_tangent(CENTER, self.bumper)
        self.direction = self.bumper + (10 * fwd_vector)
        angle_tangent_direction = find_angle(tangent, self.direction, self.bumper)
        self.heading = -(round(self.steer_angle + angle_tangent_direction, 2))
        self.toward_RA = True if self.heading > 0 else False
        speed_x, speed_y = actor.get_velocity().x, actor.get_velocity().y
        self.speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
        self.speed_km = round(3.6 * self.speed, 2)
        distance_to_center = getDistance(self.bumper, CENTER)
        self.dist_ra = distance_to_center - R_BIG
        self.status = "entered" if self.bumper_in_ra and self.status == "not-entered" else self.status
        self.in_box = True if (self.toward_RA and (0 < self.dist_ra < 10)) else False
        self.update_Arc(debug)
        self.update_Zone(debug)
        #if self.zone is not None:
            # debug.draw_line(self.zone.back.location, self.zone.front.location, color=COLOR_CARLA_RED, life_time=0.5)
            # debug.draw_string(self.zone.back.location, "b", False, carla.Color(0, 255, 0), 0.05)
            # debug.draw_string(self.zone.front.location, "f", False, carla.Color(0, 255, 0), 0.05)
        self.wp_index = get_nearest_index(self.transform, self.list_wps, self.wp_index, self.bumper)

        debug.draw_string(self.rear, str(int(self.speed_km)), False, carla.Color(255, 255, 0), 0.1)

        # if self.arc is None:
        #     distance_to_center = getDistance(self.bumper, CENTER)
        #     # self is in attention area
        #     if R_BIG < distance_to_center < R_BIG + distance_attention and self.toward_RA:
        #         self.dist_ra = distance_to_center - R_BIG
        #         self.bumper_in_ra = False
        #     if self.arc_index_current != -1:
        #         self.status = "exited"
        #         self.array_prediction = []
        #     self.arc_index_current = -1
        # else:  # vehicle entered roundabout
        #     #print(self.arc)
        #     self.lane = None
        #     self.dist_ra = -1
        #     self.bumper_in_ra = True
        #     #self.dist_exit = getDistance(self.bumper, self.arc.front.location)
        #     direct_self_exit = getDistance(self.bumper, self.arc.front)
        #     direct_exit_exit = getDistance(self.arc.back, self.arc.front)
        #     if direct_self_exit > D_BIG:direct_self_exit = D_BIG # to fix problem ValueError: math domain error
        #     self.dist_exit = D_BIG * math.asin(direct_self_exit/D_BIG)
        #
        #     self.dist_exit_normalized = round((self.dist_exit / self.arc.total_distance), 2)
        #     if self.arc_index_current == -1:
        #         self.status = "entered"
        #         self.arc_index_current = self.arc_index_new
        #     elif self.arc_index_current != self.arc_index_new:  # new Arc
        #         self.status = "not-exited"
        #         self.arc_index_current = self.arc_index_new
        #         self.episode += 1
        #     else:
        #         self.status = "in"
        #
        #     if self.prob_exit > PROB_THRESHOLD_EXIT and self.dist_exit_normalized < 0.4:
        #         if not self.isExiting:
        #             self.isExiting = True
        #             self.decision_dist = self.dist_exit
        #             self.decision_dist_norm = self.dist_exit_normalized
        #             self.decision_prob = self.prob_exit
        #             self.decision_speed = self.speed
        #     self.array_probability.append(self.prob_exit)
        #     self.array_distance.append(self.dist_exit_normalized)
        #     self.array_prediction.append([self.dist_exit_normalized, self.prob_exit])

        self.front_vehicle = None
        self.coming_vehicle = None



        self.yield_to_veh = None
        message = "go"

        #region get ahead
        self.ahead = None
        actors = world.get_actors().filter('vehicle.tesla.model3')
        others = [x for x in actors if x.id != self._id and getDistance(getRear(x), self.bumper) < Distance_Ahead_Safety]
        for other in others:
            last_index = self.wp_index + round(Distance_Ahead_Safety/nextInterval)
            for i in range(self.wp_index, last_index):
                if i > len(self.list_wps) - 1: break
                my_next_location = self.list_wps[i].wp.transform.location
                other_location = other.get_transform().location
                if getDistance(my_next_location, other_location) < LANE_WIDTH/2:     # it was 2 instead of length

                    other_speed = getSpeed(other)[0]
                    if other_speed < self.speed or abs(self.speed - other_speed) < 0.5:
                        self.ahead = other

                        if self.in_box and bumper_in_roundabout(other):
                            self.ahead = None

                    break

        if self.ahead:
            debug.draw_line(self.bumper, getRear(self.ahead), thickness=0.1, color=COLOR_CARLA_BLUE,life_time=0.1)
            message = "obstacle"
        #endregion


        if self.in_box and message != "obstacle":
            actors = world.get_actors().filter('vehicle.tesla.model3')
            #others = [x for x in actors if x.id != self._id and bumper_in_roundabout(x) and find_angle(getBumper(x), self.centroid, CENTER) < 0]
            others = [x for x in actors if x.id != self._id and bumper_in_roundabout(x) and find_angle(getBumper(x), self.rear ,self.bumper) < 10]
            others_1 = [x for x in actors if x.id != self._id and bumper_in_roundabout(x) and find_angle(getBumper(x), self.centroid, CENTER) < 10 and self.side == "left"]
            others = others + others_1

            if len(others) > 0:
                smallest_d = float('inf')
                coming_veh = None
                for other in others:
                    d = getDistance(self.bumper, getBumper(other))
                    if d < smallest_d :
                        smallest_d = d
                        coming_veh = other

                debug.draw_line(self.bumper, getBumper(coming_veh), thickness=0.15, color=COLOR_CARLA_WHITE, life_time=0.1)
                coming_speed =  math.sqrt(coming_veh.get_velocity().x ** 2 + coming_veh.get_velocity().y ** 2)
                index_between = self.idx_box_exit - self.idx_box_entry
                time_between = (self.time_entrybox_toSpot - self.time_exitbox_toSpot)
                time_interpolated_to_spot = self.time_exitbox_toSpot + time_between  * ((self.idx_box_exit - self.wp_index) / index_between)
                # print(f"time_interpolated_to_spot: {time_interpolated_to_spot}")
                speed_at_spot = self.list_wps[self.spot_wp_index].v_max_turn / 3.6
                time_to_pass_vehicle_length = self.length / speed_at_spot
                # print(f"time to pass vehicle length: {time_to_pass_vehicle_length}")
                time_required = (time_interpolated_to_spot + time_to_pass_vehicle_length)
                arc_safe_d = time_required * coming_speed
                safe_p = get_safePoint_coor(self.spot_location, arc_safe_d)

                if self.side == "right": debug.draw_string(safe_p, "p", False, carla.Color(255, 255, 0), 0.25)
                if self.side == "top":   debug.draw_string(safe_p, "p", False, carla.Color(0, 255, 0), 0.25)
                if self.side == "left":  debug.draw_string(safe_p, "p", False, carla.Color(0, 255, 255), 0.25)
                if self.side == "bottom":debug.draw_string(safe_p, "p", False, carla.Color(255, 0, 0), 0.25)

                min_safe_d  = getDistance(self.bumper, safe_p)
                if (SF * min_safe_d) < smallest_d:
                    message = "go"
                else:
                    # Applying REP anticipation model
                    message = "brake"
                    coming_bumper =  getBumper(coming_veh)
                    coming_lateral = getLateral(coming_veh)
                    coming_heading = getHeading(coming_veh)
                    coming_normalisedDistance = getNormalisedDistance(coming_veh)
                    coming_speed_km = getSpeed(coming_veh)[1]

                    # input_data = np.array([[coming_lateral, coming_heading, coming_normalisedDistance, coming_speed_km]])
                    input_data = np.array(
                        [[coming_lateral, coming_heading, coming_normalisedDistance]])
                    prob_exit = round(model.predict_proba(input_data)[0][1], 3)
                    if prob_exit > 0.7:
                        message = "go"
                        debug.draw_string(self.bumper, "informed by ML" , False, COLOR_CARLA_PINK, 0.05)
                        debug.draw_string(coming_bumper, "EXITING", False, COLOR_CARLA_PINK, 0.05)

                    #debug.draw_string(coming_bumper, str(input_data), False, COLOR_CARLA_GREEN, 0.05)

        if message == "go":
            self.brake = 0
            self.brake_index = 0
        elif message =="obstacle":
            distance2front = getDistance(self.bumper, getRear(self.ahead))
            [ahead_speed,ahead_speed_km]  = getSpeed(self.ahead)
            if distance2front > 8:
                message = "follow"
                self.current_target_velocity = ahead_speed_km
                #self.throttle_list = []

            elif distance2front <= 8:
                if self.bumper_in_ra:
                    self.brake = BRAKE_LIST[self.brake_index] if self.brake_index < len(BRAKE_LIST) else BRAKE_LIST[-1]
                    self.brake_index += 1
                    if self.brake_index > 15: self.brake_index = 5
                else:
                    if distance2front > self.length:
                        # self.throttle = []
                        self.brake = BRAKE_LIST[self.brake_index] if self.brake_index < len(BRAKE_LIST) else BRAKE_LIST[-1]
                        self.brake_index += 1
                    else:
                        self.brake = 1

        elif message =="brake":
            self.throttle = 0
            self.brake = BRAKE_LIST[self.brake_index] if self.brake_index < len(BRAKE_LIST) else BRAKE_LIST[-1]
            self.brake_index += 1


        # Move Vehicle in Box closer to RA (enforced way)
        index_between = self.idx_box_exit - self.idx_box_entry
        if not self.ahead and message == "brake" and ((self.idx_box_exit - self.wp_index) / index_between) > 0.3 \
            and self.speed_km < 5 and self.in_box:
            debug.draw_string(self.bumper, "move", False, carla.Color(255, 0, 0), 0.05)
            self.throttle = 0.25
            self.brake = 0
            self.brake_index = 0



        if len(self.throttle_list) == 0 or self.throttle_index_current == len(self.throttle_list):
            self.throttle_list = adv_thr_ctrl(self.throttle, self.speed, self.current_target_velocity,10,3,0.1)[0]
            self.throttle_index_current = 0

        new_list = self.list_wps[self.index_front:self.index_front + int(LOOK_AHEAD_DIST / nextInterval)]  # 25m fwd look
        self.current_target_velocity = min([x.v_max_turn for x in new_list])
        #     self.calling_matlab = 1
        # else: self.calling_matlab = 0

        self.counter_steer += 1
        self.throttle = self.throttle_list[self.throttle_index_current]

        if self.counter_steer > SPT-1:
            self.throttle_index_current += 1
            self.counter_steer = 0

        #region "Calculate Steer"
        center_axle_x, center_axle_y = self.bumper.x, self.bumper.y
        self.index_back = get_nearest_index(self.transform, self.list_wps, self.index_back, self.bumper)
        self.index_front = self.index_back + 1
        back_x, back_y = self.list_wps[self.index_back].transform.location.x, self.list_wps[
            self.index_back].transform.location.y
        front_x, front_y = self.list_wps[self.index_front].transform.location.x, self.list_wps[
            self.index_front].transform.location.y
        yaw_path = np.arctan2(front_y - back_y, front_x - back_x)
        yaw_diff = yaw_path - math.radians(self.yaw)
        if yaw_diff > np.pi:   yaw_diff -= 2 * np.pi
        if yaw_diff < - np.pi: yaw_diff += 2 * np.pi
        speed_x, speed_y = actor.get_velocity().x, actor.get_velocity().y
        v = math.sqrt(speed_x ** 2 + speed_y ** 2)
        crosstrack_error = DistancePointLine(center_axle_x, center_axle_y, back_x, back_y, front_x, front_y)
        yaw_cross_track = np.arctan2(center_axle_y - front_y, center_axle_x - front_x)
        yaw_path2ct = yaw_path - yaw_cross_track
        if yaw_path2ct > np.pi:   yaw_path2ct -= 2 * np.pi
        if yaw_path2ct < - np.pi: yaw_path2ct += 2 * np.pi
        if yaw_path2ct > 0:
            crosstrack_error = abs(crosstrack_error)
        else:
            crosstrack_error = - abs(crosstrack_error)
        yaw_diff_crosstrack = np.arctan(k_e * crosstrack_error / (k_v + v))
        steer_expect = yaw_diff + yaw_diff_crosstrack
        if steer_expect > np.pi:   steer_expect -= 2 * np.pi
        if steer_expect < - np.pi: steer_expect += 2 * np.pi
        steer_expect = min(self.max_steering_angle_rad, steer_expect)
        steer_expect = max(-self.max_steering_angle_rad, steer_expect)
        steer_normalized = steer_expect / self.max_steering_angle_rad

        # tire_friction= round(self.tire_friction,2)
        # radius = round(self.wheels_raduis,2)
        # max_brake_torque = round(self.max_brake_torque,2)
        # print(f"Veh({self._id}): [tire_friction:{tire_friction}],[radius:{radius}],[max_brake_torque:{max_brake_torque}]")

        #endregion

        self.steer = steer_normalized
        self.setControl(actor)


    def get_ahead_actor(self,world):
        actors = world.get_actors().filter('vehicle.tesla.model3')
        others = [x for x in actors if getDistance(getRear(x),self.bumper) < Distance_Ahead_Safety]
        for other in others:
            #world.debug.draw_line(self.bumper, other.get_transform().location, thickness=0.1, color=COLOR_CARLA_RED, life_time=0.1)
            for i in range(self.wp_index, self.wp_index + 100 ):#int(Distance_Ahead_Safety/nextInterval)
                world.debug.draw_line(self.bumper, getRear(other), thickness=0.12, color=COLOR_CARLA_BLUE,
                                      life_time=0.1)
                if i > len(self.list_wps)-1: break
                my_next_location = self.list_wps[i].wp.transform.location
                other_location = other.get_transform().location
                if getDistance(my_next_location,other_location) < (LANE_WIDTH/2):
                    return other
        return None


    def plot(self):
        plt.plot(self.array_distance, self.array_probability, c="#FF8888", label="exited")
        plt.axvline(x=self.decision_dist_norm, color='blue')
        plt.xlabel("Distance")
        plt.ylabel("Probability")
        plt.legend()
        plt.show()
    def calculateBrake(self):
        ratio_distance = 1- (self.dist_ra/distance_attention)
        ratio_speed = self.speed_km/speed_limit
        brake = 0.2 * ratio_distance + 0.2 * ratio_speed + 0.6 * self.brake
        return brake
    def calculateThrottle(self):
        throttle = 0.05 + (0.45 * self.throttle) + (0.5 * self.wp_throttle)
        throttle = min(throttle, 1)
        return throttle
    def setControl(self,actor):
        control = actor.get_control()
        control.steer = self.steer
        control.throttle = self.throttle # 0.5 #self.calculateThrottle()
        control.brake = self.brake
        #if control.brake > 0.1:print(f"{self._id} is brake: {round(control.brake,2)} and Throttle: {round(control.throttle,2)}")
        actor.apply_control(control)
    def get_newtransform(self, world, actors):
        new_transforms = []
        for t in get_spawnpoints(world, replacing=True):
            closest_veh = 1000
            for actor in actors:
                new_distance = getDistance(actor.get_transform().location, t.location)
                if new_distance < closest_veh:
                    closest_veh = new_distance
            if closest_veh > 6:
                new_transforms.append(tuple([closest_veh, t]))
        lenght = len(new_transforms)

        if lenght > 0:
            new_transforms.sort(key=lambda x: x[0], reverse=True)
            return new_transforms[0][1]
        else:
            return None

        list_vehicleSpeeds.append(vehSpeeds)
        jsonstr1 = json.dumps(vehSpeeds.__dict__)
        del self

def plot(array_steps,array_steer, timestamp, k_constant):
    plt.plot(array_steps, array_steer, c="#FF8888", label="exited")
    plt.title(f"{k_constant}")
    plt.xlabel(f"steps: [{timestamp}] ")
    plt.ylabel("steering")
    plt.legend()
    plt.show()

def get_polygones(debug):
        circles = []
        circle_1 = []
        circle_2 = []
        countd = 1
        radius_1 = R_SMALL + (2 * LANE_WIDTH)
        radius_2 = R_SMALL + (2 * LANE_WIDTH) +10
        for i in range(360):
            angle = countd * (math.pi / 180)
            x1,y1 = radius_1 * math.sin(angle), radius_1 * math.cos(angle)
            x2,y2 = radius_2 * math.sin(angle), radius_2 * math.cos(angle)
            p1 = carla.Location(x1 + X_CENTER, y1 + Y_CENTER)
            p2 = carla.Location(x2 + X_CENTER, y2 + Y_CENTER)
            circle_1.append(p1)
            circle_2.append(p2)
            countd += 1
        circles.append(circle_1)
        circles.append(circle_2)

        # debug.draw_string(circle_2[0], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[12], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[94], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[106], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[181], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[193], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[267], "@", False, carla.Color(255, 0, 0), 130)
        # debug.draw_string(circle_2[274], "@", False, carla.Color(255, 0, 0), 130)

        for i in range(len(circle_1)-1):
            if (i >5 and i< 35) or ( i > 96 and i < 122) or ( i> 185 and i< 213) or ( i> 273 and i< 292 ):
                p1, p2 = circle_1[i], circle_1[i+1]
                debug.draw_line(p1, p2,thickness=0.3, color=COLOR_CARLA_RED, life_time=10000)

        for i in range(len(circle_2) - 1):
            if (i > -1 and i < 12) or (i > 94 and i < 106) or (i > 181 and i < 193) or (i > 267 and i < 274):
                p1, p2 = circle_2[i], circle_2[i + 1]
                debug.draw_line(p1, p2, thickness=0.3, color=COLOR_CARLA_RED, life_time=10000)


def spawn(side, world, blueprints, debug, occurency = 1):
    global parking_x
    if side == "right":
        ego_t = get_right_spawnpoint(world)
        # if occurency == 2: ego_t = carla.Transform(ego_t.location + carla.Location(y=10), ego_t.rotation)
        if occurency % 2 != 0: ego_t = carla.Transform(ego_t.location + carla.Location(y=21), ego_t.rotation)
        else: ego_t = carla.Transform(ego_t.location + carla.Location(y=29), ego_t.rotation)
        # if occurency == 3: ego_t = carla.Transform(ego_t.location + carla.Location(y=20), ego_t.rotation)
        # if occurency == 4: ego_t = carla.Transform(ego_t.location + carla.Location(y=30), ego_t.rotation)
        color = '255, 255, 0'
    elif side == "top":
        ego_t = get_top_spawnpoint(world)
        if occurency % 2 != 0:
            ego_t = carla.Transform(ego_t.location + carla.Location(x=21), ego_t.rotation)
        else:
            ego_t = carla.Transform(ego_t.location + carla.Location(x=29), ego_t.rotation)
        color = '0, 255, 0'
    elif side == "left":
        ego_t = get_left_spawnpoint(world)
        if occurency % 2 != 0:
            ego_t = carla.Transform(ego_t.location + carla.Location(y=-21), ego_t.rotation)
        else:
            ego_t = carla.Transform(ego_t.location + carla.Location(y=-29), ego_t.rotation)
        color = '0, 255, 255'
    else:
        ego_t = get_bottom_spawnpoint(world)
        if occurency % 2 != 0:
            ego_t = carla.Transform(ego_t.location + carla.Location(x=-11), ego_t.rotation)
        else:
            ego_t = carla.Transform(ego_t.location + carla.Location(x=-19), ego_t.rotation)
        color = '255, 0, 0'
    ego_bp = blueprints.find('vehicle.tesla.model3')
    ego_bp.set_attribute('color', color)

    success = False
    # debug.draw_string(ego_t.location, "H", False, carla.Color(255, 0, 0), 130)

    while not success:
            try:
                parking_t = carla.Transform(carla.Location(parking_x, parking_y, 1.5), carla.Rotation(pitch=0, roll=0, yaw=0))
                actor_ego = world.spawn_actor(ego_bp, parking_t)
                parking_x += 7
                success = True
            except Exception:
                parking_x += 7
                # if side == "right": ego_t = carla.Transform(ego_t.location + carla.Location(y=10), ego_t.rotation)
                # if side == "top": ego_t = carla.Transform(ego_t.location + carla.Location(x=10), ego_t.rotation)
                # if side == "left": ego_t = carla.Transform(ego_t.location + carla.Location(y=-10), ego_t.rotation)
                # if side == "bottom": ego_t = carla.Transform(ego_t.location + carla.Location(x=-10), ego_t.rotation)
                print("Ex......1")

    try:
        actor_ego.set_autopilot(False)
        ########################################################
        veh_ego = Vehicle(side, actor_ego)
        veh_ego.initial_transform = ego_t
        #wp_index, wp = get_nearest_wp(actor_ego.get_location(), wps)
        wp_index, wp = get_nearest_wp(ego_t.location, wps)
        veh_ego.list_wps = []
        while wp is not None:
            myWaypoint = MyWaypoint(wp)
            veh_ego.list_wps.append(myWaypoint)

            location = wp.transform.location
            if max(abs(location.x), abs(location.y)) > MAX_EDGE: break

            nexts = wp.next(nextInterval)
            # if side == "right": debug.draw_string(wp.transform.location, "*", False, carla.Color(255, 255, 0), 30)
            # if side == "top":   debug.draw_string(wp.transform.location, "*", False, carla.Color(0, 255, 0), 30)
            # if side == "left":  debug.draw_string(wp.transform.location, "*", False, carla.Color(0, 255, 255), 30)
            # if side == "bottom":debug.draw_string(wp.transform.location, "*", False, carla.Color(255, 0, 0), 30)

            if len(nexts) == 2:  wp = nexts[random.randint(0, 1)]
            elif len(nexts) > 0: wp = nexts[0]
            else: wp = None
        veh_ego.spot_wp_index =  getVehicleSpot_wp_index( veh_ego.list_wps, veh_ego.target_zone_location)
        for i in range(len(veh_ego.list_wps) - int(dist_curvature_check/nextInterval)):
            if (i % int(dist_curvature_check/nextInterval)) !=0: continue
            array_next_10wp = veh_ego.list_wps[i : i + num_point_circle_fit + 1]
            tuples_next_10wp = [(wp.transform.location.x, wp.transform.location.y) for wp in array_next_10wp]
            x_c, y_c, r, _ = cf.least_squares_circle(tuples_next_10wp)
            for j in range(int(dist_curvature_check/nextInterval)):
                try:
                    veh_ego.list_wps[i+j].radius = r
                    veh_ego.list_wps[i+j].v_max_turn = int(3.6 * min(math.sqrt(ACC_LATERAL * r), (TARGET_VELOCITY / 3.6)))
                    veh_ego.list_wps[i+j].circle_center = Point(x_c, y_c)
                except:
                    print(i)

        if veh_ego.side == "right":
            box_entry = box_right_entry
            box_exit = box_right_exit
        elif veh_ego.side == "top":
            box_entry = box_top_entry
            box_exit = box_top_exit
        elif veh_ego.side == "left":
            box_entry = box_left_entry
            box_exit = box_left_exit
        else:
            box_entry = box_bottom_entry
            box_exit = box_bottom_exit

        veh_ego.idx_box_entry,_ = get_nearest_wp(box_entry, veh_ego.list_wps)
        for i in range(max(0,veh_ego.idx_box_entry-int(CAUTION_DIST_BEFORE_BOX/nextInterval)),veh_ego.idx_box_entry):
            prev = veh_ego.list_wps[i].v_max_turn
            veh_ego.list_wps[i].v_max_turn = 15 #km/h


        List_distances_at_v0 = adv_thr_ctrl(0, 0, veh_ego.list_wps[veh_ego.spot_wp_index].v_max_turn, 10, 3, 0.1)[3]

        veh_ego.distance_entrybox_toSpot = (veh_ego.spot_wp_index - veh_ego.idx_box_entry) * nextInterval
        for i,d in enumerate(List_distances_at_v0):
            if d > veh_ego.distance_entrybox_toSpot:
                index = i
                veh_ego.time_entrybox_toSpot = index * Delta_Time
                break

        veh_ego.idx_box_exit, _ = get_nearest_wp(box_exit, veh_ego.list_wps)
        veh_ego.distance_exitbox_toSpot = (veh_ego.spot_wp_index - veh_ego.idx_box_exit) * nextInterval
        for i, d in enumerate(List_distances_at_v0):
            if d > veh_ego.distance_exitbox_toSpot:
                index = i
                veh_ego.time_exitbox_toSpot = index * Delta_Time
                break


        # print(f"Entry box: {veh_ego.side} {veh_ego.time_entrybox_toSpot}")
        # print(f"Exit box: {veh_ego.side} {veh_ego.time_exitbox_toSpot}")
        # debug.draw_string(ego_t.location, str(veh_ego.time_entrybox_toSpot), False, carla.Color(0, 0, 0), 1000)
        # debug.draw_string(-15 * (ego_t.get_forward_vector()), str(veh_ego.time_exitbox_toSpot), False, carla.Color(0, 0, 200), 1000)
    except Exception:
        traceback.print_exc()
        pass

    return veh_ego, actor_ego

def start_vehicles():
    global actors, waiting_vehicles
    while len(waiting_vehicles) > 0:
        filter_right = [x for x in waiting_vehicles if x.side == "right"]
        filter_top = [x for x in waiting_vehicles if x.side == "top"]
        filter_left = [x for x in waiting_vehicles if x.side == "left"]
        filter_bottom = [x for x in waiting_vehicles if x.side == "bottom"]

        veh_right_1 = filter_right.pop(0)
        veh_right_2 = filter_right.pop(0)
        veh_top_1 = filter_top.pop(0)
        veh_top_2 = filter_top.pop(0)
        veh_left_1 = filter_left.pop(0)
        veh_left_2 = filter_left.pop(0)
        veh_bottom_1 = filter_bottom.pop(0)
        veh_bottom_2 = filter_bottom.pop(0)
        actor_right_1 = [x for x in actors if x.id == veh_right_1._id][0]
        actor_right_2 = [x for x in actors if x.id == veh_right_2._id][0]
        actor_top_1 = [x for x in actors if x.id == veh_top_1._id][0]
        actor_top_2 = [x for x in actors if x.id == veh_top_2._id][0]
        actor_left_1 = [x for x in actors if x.id == veh_left_1._id][0]
        actor_left_2 = [x for x in actors if x.id == veh_left_2._id][0]
        actor_bottom_1 = [x for x in actors if x.id == veh_bottom_1._id][0]
        actor_bottom_2 = [x for x in actors if x.id == veh_bottom_2._id][0]

        actor_right_1.set_transform(veh_right_1.list_wps[0].transform)
        actor_right_2.set_transform(veh_right_2.list_wps[0].transform)
        actor_top_1.set_transform(veh_top_1.list_wps[0].transform)
        actor_top_2.set_transform(veh_top_2.list_wps[0].transform)
        actor_left_1.set_transform(veh_left_1.list_wps[0].transform)
        actor_left_2.set_transform(veh_left_2.list_wps[0].transform)
        actor_bottom_1.set_transform(veh_bottom_1.list_wps[0].transform)
        actor_bottom_2.set_transform(veh_bottom_2.list_wps[0].transform)
        time.sleep(3.5)
        vehicles.append(veh_right_1)
        time.sleep(0.5)
        vehicles.append(veh_top_1)
        time.sleep(0.5)
        vehicles.append(veh_left_1)
        time.sleep(0.5)
        vehicles.append(veh_bottom_1)
        time.sleep(2)

        vehicles.append(veh_right_2)
        time.sleep(0.5)
        vehicles.append(veh_top_2)
        time.sleep(0.5)
        vehicles.append(veh_left_2)
        time.sleep(0.5)
        vehicles.append(veh_bottom_2)
        time.sleep(115)

        for i in range(8):
            waiting_vehicles.pop(0)



array_drawn_others, wps, vehicles, waiting_vehicles = [],[],[],[]
def main():
    global timestamp, Arcs, Zones, actors, BRAKE_LIST
    global initial_transform, timeStep, REP_stay_max
    global wps, vehicles, waiting_vehicles
    global parking_x
    loop = True
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    global ego_id, ego_actor,veh_ego

    taw_brake = 1 / (math.log(20))  # 95% response at 1 sec
    BRAKE_LIST = [round(-0.375 * math.exp(-i * (Delta_Time/SPT) / taw_brake) + 0.375, 4) for i in range(round(50 / Delta_Time))]
    BRAKE_LIST.pop(0)

    client = carla.Client(args.host, args.port)
    client.set_timeout(5.0)

    world = client.get_world()

    try:
        #region Global-Settings
        map = world.get_map()
        debug = world.debug
        get_polygones(debug)

        wps = map.generate_waypoints(0.5)
        spectator = world.get_spectator()
        spectator.set_transform(carla.Transform(carla.Location(x=0, y=0, z=80),carla.Rotation(pitch=-90)))
        traffic_manager = client.get_trafficmanager(tm_port)
        traffic_manager.set_synchronous_mode(True)
        blueprints = world.get_blueprint_library()

        _exit_top = carla.Location(x=TOP_exit_X, y=TOP_exit_Y, z=0.5)
        _exit_right = carla.Location(x=RIGHT_exit_X, y=RIGHT_exit_Y, z=0.5)
        _exit_bottom = carla.Location(x=BOTTOM_exit_X, y=BOTTOM_exit_Y, z=0.5)
        _exit_left = carla.Location(x=LEFT_exit_X, y=LEFT_exit_Y, z=0.5)
        arc_top_right = Arc("Top_Right", _exit_right, _exit_top)
        arc_right_bottom = Arc("Right_Bottom", _exit_bottom, _exit_right)
        arc_bottom_left = Arc("Bottom_Left", _exit_left, _exit_bottom)
        arc_left_top = Arc("Left_Top", _exit_top, _exit_left)

        # zone_top_right = Arc("Top_Right",  entry_right,entry_top, "right")
        # zone_right_bottom = Arc("Right_Bottom", entry_bottom, entry_right, "bottom")
        # zone_bottom_left = Arc("Bottom_Left", entry_left, entry_bottom, "left")
        # zone_left_top = Arc("Left_Top",  entry_top,entry_left, "top")

        top = carla.Location(CENTER.x + R_BIG,CENTER.y,2)
        right = carla.Location(CENTER.x ,CENTER.y+ R_BIG,2)
        bottom = carla.Location(CENTER.x-R_BIG ,CENTER.y,2)
        left = carla.Location(CENTER.x,CENTER.y - R_BIG,2)

        # debug.draw_string(exit_top.location, "exit", False, carla.Color(0, 255, 0, 0), 500)
        # debug.draw_string(exit_right.location, "exit", False, carla.Color(0, 255, 0, 0), 500)
        # debug.draw_string(exit_bottom.location, "exit", False, carla.Color(0, 255, 0, 0), 500)
        # debug.draw_string(exit_left.location, "exit", False, carla.Color(0, 255, 0, 0), 500)

        Arcs = [arc_top_right, arc_right_bottom, arc_bottom_left, arc_left_top]
        # debug.draw_line(CENTER,arc_top_right.front, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(arc_top_right.back,CENTER, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(CENTER,arc_right_bottom.front, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(arc_right_bottom.back,arc_right_bottom.front, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(arc_bottom_left.back,arc_bottom_left.front, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(arc_bottom_left.back,arc_bottom_left.front, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(arc_left_top.back,arc_left_top.front, color=COLOR_CARLA_RED, life_time=500)
        # debug.draw_line(arc_left_top.back,arc_left_top.front, color=COLOR_CARLA_RED, life_time=500)



        #Zones = [zone_top_right, zone_right_bottom, zone_bottom_left, zone_left_top]

        spawn_points = get_spawnpoints(world)
        random.shuffle(spawn_points)
        #endregion

        sides = ["right","top","left","bottom"]

        for i in range(1,3):
            try:
                veh_ego_right, actor_ego_right = spawn("right", world, blueprints, debug,i)
                waiting_vehicles.append(veh_ego_right)

                veh_ego_top, actor_ego_top = spawn("top", world, blueprints, debug,i)
                waiting_vehicles.append(veh_ego_top)

                veh_ego_left, actor_ego_left = spawn("left", world, blueprints, debug,i)
                waiting_vehicles.append(veh_ego_left)

                veh_ego_bottom, actor_ego_bottom = spawn("bottom", world, blueprints, debug,i)
                waiting_vehicles.append(veh_ego_bottom)
            except Exception:
                traceback.print_exc()
                pass

        timestep = 0
        #debug.draw_string(wp_spot.transform.location, "*", False, COLOR_CARLA_GREEN , 300)
        initial_timestamp = world.get_snapshot().timestamp.platform_timestamp

        array_steps,array_steer = [], []
        counter = 0
        # debug.draw_string(carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5), "S_r", False,carla.Color(255, 0, 0), 500)
        # debug.draw_string(carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5), "S_t", False,carla.Color(255, 0, 0), 500)
        # debug.draw_string(carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5), "S_l", False,carla.Color(255, 0, 0), 500)
        # debug.draw_string(carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5), "S_b", False,carla.Color(255, 0, 0), 500)

        S_safety_right = get_safePoint_coor(carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5), 25)
        # debug.draw_string(S_safety_right, "S_R", False, carla.Color(255, 0, 0),500)

        previous_time = world.get_snapshot().timestamp.platform_timestamp
        current_time = world.get_snapshot().timestamp.platform_timestamp
        clock = pygame.time.Clock()
        counter = 0
        counter_vehicles = 0

        actors = world.get_actors().filter('vehicle.tesla.model3')
        threading.Timer(0, start_vehicles, ()).start()

        while loop:
            current_time = world.get_snapshot().timestamp.platform_timestamp
            counter += 1

            actors = world.get_actors().filter('vehicle.tesla.model3')
            timeDifference = current_time - previous_time
            for veh in vehicles:
                try:
                    actors = world.get_actors().filter('vehicle.tesla.model3')
                    filtered_actors = [x for x in actors if x.id == veh._id]
                    if len(filtered_actors) == 0: continue
                    curr_actor = filtered_actors[0]
                    veh.update(curr_actor, vehicles,world, debug)
                    if max(abs(veh.centroid.x), abs(veh.centroid.y)) > 80:
                        try:
                            side = veh.side
                            vehicles.remove(veh)
                            del veh
                            curr_actor.destroy()

                            # actor_ego = spawn_carla_vehicle(side, world, blueprints, debug)
                            # #thread.start_new_thread(thread_spawned_config, (side, world, blueprints, debug))
                            # threading.Timer(0,thread_spawned_config, (side,actor_ego,)).start()
                            # print(f" Respawned: " + str(timeDifference))
                            continue
                        except Exception:
                            traceback.print_exc()
                            pass

                    #print(veh.toward_RA)
                    #debug.draw_string(veh.centroid, str(veh.in_box), False, carla.Color(255, 255, 0), 0.05)
                except Exception:
                        traceback.print_exc()
                        pass

            clock.tick_busy_loop(int(SPT / Delta_Time))
            previous_time = current_time

            actors_alive = world.get_actors().filter('vehicle.tesla.model3')
            # if len(actors_alive) == 4:
            #     loop = False


        print("Simulation ended")
        print(REP_stay_max)
    finally:
        print("Finally Ended..............")
        print(REP_stay_max)
        # for veh in vehicles:
        #     actor = world.get_actor(veh._id)
        #     if actor is not None:
        #         actor.destroy()

if __name__ == '__main__':
    main()
